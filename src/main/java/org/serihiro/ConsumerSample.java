package org.serihiro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by seri on 11/23/15.
 */

public class ConsumerSample {
    SimpleConsumer consumer;

    public ConsumerSample() {
        this.consumer = new SimpleConsumer();
    }

    public <T> List removeFirstElement(ArrayList<T> list) {
        Consumer<ArrayList<T>> removeFistLambda = (consumerInput) -> {
            consumerInput.remove(0);
        };
        List<Integer> list2 = Arrays.asList(1, 2, 3);
        Integer result = 0;
        for (Integer i : list2) {
            result += i * i;
        }
        consumer.run(removeFistLambda, list);
        return list;
    }



    class SimpleConsumer {
        public <T> void run(Consumer<T> c, T t) {
            c.accept(t);
        }
    }
}
