package org.serihiro;

import java.lang.String;
import java.lang.System;
import java.util.ArrayList;

/**
 * Hello world!
 */
public class Hello {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Hello!");
        list.add("Hello!");
        list.add("Hello!");
        list.forEach(System.out::println);
    }

    public String sayGoodBye() {
        return "Good Bye";
    }
}
