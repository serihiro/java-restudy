package org.serihiro;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by seri on 11/21/15.
 */
public class SortWithLambdaSample {
    public static List<Integer> sortIntegerList(List<Integer> integerList, Comparator<Integer> c) {
        integerList.sort(c);
        return integerList;
    }

    public static Integer[] sortIntegerArray(Integer[] integerArray, Comparator<Integer> c) {
        Arrays.sort(integerArray, c);
        return integerArray;
    }
}
