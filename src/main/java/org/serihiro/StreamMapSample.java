package org.serihiro;

import java.util.List;

/**
 * Created by seri on 11/24/15.
 */
public class StreamMapSample {
    public static Integer calcSumOfSquare(List<Integer> list) {
        Integer result = 0;
        for (Integer i : list) {
            result += i * i;
        }
        return result;
    }

    public static Integer calcSumOfSquareWithStream(List<Integer> list) {
        return list
                .stream()
                .map(x -> x * x)
                .reduce((a, b) -> a + b)
                .get();
    }
}
