package org.serihiro;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class HelloTest
        extends TestCase {

    private Hello hello;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public HelloTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(HelloTest.class);
    }

    @Override
    protected void setUp() {
        this.hello = new Hello();
    }

    public void testApp() {
        assertEquals(hello.sayGoodBye(), "Good Bye");
    }
}
