package org.serihiro;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

/**
 * Created by seri on 11/24/15.
 */
public class StreamMapSampleTest extends TestCase {
    public void testCalcSumOfSquareWithStream() throws Exception {
        List<Integer> list = Arrays.asList(1, 2, 3);
        assertEquals(StreamMapSample.calcSumOfSquareWithStream(list), new Integer(14));
    }

    public void testCalcSumOfSquare() throws Exception {
        List<Integer> list = Arrays.asList(1, 2, 3);
        assertEquals(StreamMapSample.calcSumOfSquare(list), new Integer(14));
    }
}
