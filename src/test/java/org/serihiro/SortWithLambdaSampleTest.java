package org.serihiro;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by seri on 11/21/15.
 */
public class SortWithLambdaSampleTest extends TestCase {
    public void testSortIntegerListWithLambda() throws Exception {
        List<Integer> list = Arrays.asList(1, 3, 2);
        Comparator<Integer> c = (x, y) -> y - x;
        List<Integer> sortedList = SortWithLambdaSample.sortIntegerList(list, c);
        assertEquals(new Integer(3), sortedList.get(0));
        assertEquals(new Integer(2), sortedList.get(1));
        assertEquals(new Integer(1), sortedList.get(2));
    }

    public void testSortIntegerListWithClassicalComparatorClass() throws Exception {
        List<Integer> list = Arrays.asList(1, 3, 2);
        Comparator<Integer> c = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        };
        List<Integer> sortedList = SortWithLambdaSample.sortIntegerList(list, c);
        assertEquals(new Integer(3), sortedList.get(0));
        assertEquals(new Integer(2), sortedList.get(1));
        assertEquals(new Integer(1), sortedList.get(2));
    }

    public void testSortIntegerArray() throws Exception {
        Integer integerArray[] = {1, 3, 2};
        Comparator<Integer> c = (x, y) -> y - x;
        Integer[] sortedArray = SortWithLambdaSample.sortIntegerArray(integerArray, c);
        assertEquals(new Integer(3), sortedArray[0]);
        assertEquals(new Integer(2), sortedArray[1]);
        assertEquals(new Integer(1), sortedArray[2]);
    }
}
