package org.serihiro;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by seri on 11/23/15.
 */
public class ConsumerSampleTest extends TestCase {
    ConsumerSample subject;

    @Override
    protected void setUp() {
        this.subject = new ConsumerSample();
    }

    public void testRemoveFirstElement() throws Exception {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        ArrayList<Integer> expectList = new ArrayList<>();
        expectList.add(2);
        expectList.add(3);
        assertEquals(subject.removeFirstElement(list), expectList);
    }
}
